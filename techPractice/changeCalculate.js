// Buatlah function yang dapat digunakan oleh kasir
// untuk menghitung nilai uang kembalian beserta dengan pecahan uang yang bisa diberikan
// Input berupa total belanja dan jumlah uang yang dibayarkan oleh pembeli
// Output berupa kembalian (dibulatkan ke bawah Rp.100) yang harus diberikan kasir dengan detail pecahan uang yang harus diberikan
// Pecahan yang tersedia adalah: 100.000, 50.000, 20.000, 10.000, 5.000, 2.000, 1.000, 500, 200, 100.
// Kembalikan nilai False apabila jumlah uang yang dibayarkan kurang dari total belanjanya.

const money = [
  100000, 75000, 50000, 20000, 10000, 5000, 2000, 1000, 500, 200, 100,
];
function cassier(uang, belanja) {
  // input
  // total belanja
  // jumlah uang yang di bayar

  let kembalian = uang - belanja;
  console.log(kembalian);
  if (belanja > uang) {
    return "False";
  } else if (belanja < uang) {
    for (let i = 0; i < money.length; i++) {
      let seratus = Math.floor(kembalian / money[i]);
      kembalian = kembalian % money[i];
      console.log(`${seratus} lembar RP ${money[i]}`);
    }
  }
  // 148800
  // seratus 1
  // duapuluh 2
  // limaribu 1
  // duaribu 1
  // seribu 1
  // limaratus 1
  // seratus 3

  // output
  // bulatkan ke bawah
}

cassier(300000, 151200); //148.800
