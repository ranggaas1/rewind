for (let i = 0; i < 10; i++) {
  console.log(i);
}

let fruits = ["Apple", "Banana", "Orange", "Mango", "Melon"];
for (let i = 0; i < fruits.length; i++) {
  console.log(fruits[i]);
}

fruits.map((fruit) => console.log(fruit));
