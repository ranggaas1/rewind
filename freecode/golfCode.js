// array
var names = [
  "Hole-in-one!",
  "Eagle",
  "Birdie",
  "Par",
  "Bogey",
  "Double Bogey",
  "Go Home!",
];

// create function score golf
function scoreGolf(par, strokes) {
  // stokes 1, hole in one
  if (strokes == 1) {
    return "Hole-in-one!";
    // strokes <= par - 2, eagle
  } else if (strokes <= par - 2) {
    return "Eagle";
    // strokes par - 1, berdie
  } else if (strokes == par - 1) {
    return "Birdie";
  } else if (strokes == par) {
    return "Par";
  } else if (strokes == par + 1) {
    return "Bogey";
  } else if (strokes == par + 2) {
    return "Double Bogey";
  } else if (strokes >= par + 3) {
    return "Go Home!";
  }
  return "Change Me";
}

console.log(scoreGolf(10, 1));
