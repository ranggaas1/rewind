// give var count
var count = 0;

// make function for count card
function countCard(card) {
  // describe regex
  var regex = /[JQKA]/;
  // for cards 2, 3, 4, 5, 6
  if (card > 1 && card < 7) {
    // next card
    count++;
  }
  // if wrong
  // and cards 10, J, Q, K, A
  else if (card === 10 || regex.test(card)) {
    // retrun card
    count--;
  }

  // if count > 0 print bet
  // if else print hold
  if (count > 0) return count + " Bet";
  return count + " Hold";
}

console.log(countCard(2));
console.log(countCard(3));
console.log(countCard(7));
console.log(countCard("K"));
console.log(countCard("A"));
