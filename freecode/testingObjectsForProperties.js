var checkObj = { gift: "pony", pet: "kitten", bed: "sleigh" };

function checkObj(obj, checkProp) {
  if (obj.hasOwnProperty(checkProp)) {
    return obj[checkProp];
  } else {
    return "Not Found";
  }
}

console.log(checkObj.school);
console.log(JSON.stringify(checkObj.gift));
