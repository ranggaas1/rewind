// node module import
const express = require("express");

const {
  createStudent,
  getAllStudent,
} = require("../controllers/helloController");

// make routes
const router = express.Router();

/* If user go to (POST) http://localhost:3000/ or http://localhost:3000, it will go to here */
/* POST used to send data from client */
router.post("/", createStudent);

/* If user go to (GET) http://localhost:3000/ or http://localhost:3000, it will go to here */
/* GET used to GET the data */
router.get("/", getAllStudent);

// exports router
module.exports = router;
