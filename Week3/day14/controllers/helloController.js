const students = require("../models");

class helloController {
  createStudent(req, res) {
    try {
      students.push(req.body);

      res.students(201).json({
        data: req.body,
      });
    } catch (error) {
      res.students(500).json({
        message: error.message,
      });
    }
  }

  getAllStudent(req, res) {
    try {
      req.res(200).json({
        data: students,
      });
    } catch (error) {
      req.status(500).json({
        message: error.message,
      });
    }
  }

  getOneStudent(req, res) {
    try {
      const student = students.filter(
        (student) => student.id === eval(req.params.id)
      );

      res.status(200).json({
        data: student,
      });
    } catch (error) {
      req.status(500).json({
        message: error.message,
      });
    }
  }
}

// export
module.exports = new helloController();
