// import express
const express = require("express");

// create express app
const app = express;

// enable req.body
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

// import routes
const router = require("./routes/helloRoutes");

// user the routes
app.use("/", helloRoutes);

// run the server
app.listen(3000, () => console.log("Server running on port 3000"));
